const fs = require(`fs`);

const blockName = process.argv[2];
const PATH = `source/blocks`;
const dir = `${PATH}/${blockName}`;
const Content = {
  PUG: `\
section.${blockName}
  h2.${blockName}__title\n`,
  SCSS: `\
.${blockName} {
  // styles here
}\n`
};

fs.mkdirSync(dir);
fs.writeFileSync(`${dir}/${blockName}.pug`, Content.PUG);
fs.writeFileSync(`${dir}/${blockName}.scss`, Content.SCSS);
