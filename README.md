- `npm start` - start development
- `npm run lint` - linting js and scss
- `npm run build` - production version
- `npm run build:development` - development version
- `npm run sort` - sort rules in source scss files
- `npm run imagemin` - minify image in `source/img` folder  

Ознакомиться со стайлгайдом можно [тут](styleguide.md)
