module.exports = [
  {
    title: `Русский язык`,
    code: `russian-language-inverse`,
    imgs: [
      `img/subject-001.jpg`,
      `img/subject-001.jpg, img/subject-001@2x.jpg 2x`
    ],
    link: `subject-russian.html`
  }, {
    title: `Литература`,
    code: `literature-inverse`,
    imgs: [
      `img/subject-002.jpg`,
      `img/subject-002.jpg, img/subject-002@2x.jpg 2x`
    ],
    link: `subject-literature.html`
  }, {
    title: `История`,
    code: `history-inverse`,
    imgs: [
      `img/subject-003.jpg`,
      `img/subject-003.jpg, img/subject-003@2x.jpg 2x`
    ],
    link: `subject-history.html`
  }, {
    title: `Обществознание`,
    code: `social-science-inverse`,
    imgs: [
      `img/subject-004.jpg`,
      `img/subject-004.jpg, img/subject-004@2x.jpg 2x`
    ],
    link: `subject-social.html`
  }, {
    title: `Математика`,
    code: `mathematics-inverse`,
    mod: [`math-base`],
    dataCaption: `Базовая`,
    imgs: [
      `img/subject-010.jpg`,
      `img/subject-010.jpg, img/subject-010@2x.jpg 2x`
    ],
    link: `subject-mathematics.html`
  }, {
    title: `Математика`,
    code: `mathematics-inverse`,
    mod: [`math-profile`, `in-process`],
    data: `В разработке`,
    dataCaption: `Профильная`,
    imgs: [
      `img/subject-mathematics-profile.jpg`,
      `img/subject-mathematics-profile.jpg`
    ],
    link: `subject-mathematics-profile.html`
  }, {
    title: `Физика`,
    code: `physics-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-005.jpg`,
      `img/subject-005.jpg, img/subject-005@2x.jpg 2x`
    ],
    link: `subject-physics.html`
  }, {
    title: `Химия`,
    code: `chemistry-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-006.jpg`,
      `img/subject-006.jpg, img/subject-006@2x.jpg 2x`
    ],
    link: `subject-chemistry.html`
  }, {
    title: `Биология`,
    code: `biology-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-007.jpg`,
      `img/subject-007.jpg, img/subject-007@2x.jpg 2x`
    ],
    link: `subject-biology.html`
  }, {
    title: `География`,
    code: `geography-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-008.jpg`,
      `img/subject-008.jpg, img/subject-008@2x.jpg 2x`
    ],
    link: `subject-geography.html`
  }, {
    title: `Информатика`,
    code: `computer-science-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-009.jpg`,
      `img/subject-009.jpg, img/subject-009@2x.jpg 2x`
    ],
    link: `subject-computer.html`
  }, {
    title: `Английский`,
    code: `english-language-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-011.jpg`,
      `img/subject-011.jpg, img/subject-011@2x.jpg 2x`
    ],
    link: `subject-english.html`
  }, {
    title: `Немецкий`,
    code: `german-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-012.jpg`,
      `img/subject-012.jpg, img/subject-012@2x.jpg 2x`
    ],
    link: `subject-german.html`
  }, {
    title: `Французский`,
    code: `french-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-013.jpg`,
      `img/subject-013.jpg, img/subject-013@2x.jpg 2x`
    ],
    link: `subject-french.html`
  }, {
    title: `Испанский`,
    code: `spanish-inverse`,
    mod: [`in-process`],
    data: `В разработке`,
    imgs: [
      `img/subject-014.jpg`,
      `img/subject-014.jpg, img/subject-014@2x.jpg 2x`
    ],
    link: `subject-spanish.html`
  }
];
