module.exports = [{
  name: `Полина Мерсалова`,
  subject: [`История`],
  subjectCodes: [
    `history`
  ],
  status: [
    `Составитель`,
    `Эксперт`
  ],
  imgs: [
    `img/teacher-001.jpg`,
    `img/teacher-001.jpg, img/teacher-001@2x.jpg 2x`
  ],
  education: [
    `2010&nbsp;г. &mdash;&nbsp;золотая медаль в&nbsp;школе.`,
    `Кубанский государственный университет: 2010&ndash;2014&nbsp;&mdash; бакалавр истории {бюджет}. 2014&ndash;2016&nbsp;&mdash; магистр истории {бюджет}.`,
    `2016&nbsp;г. &mdash;&nbsp;победитель конкурса молодых учителей &laquo;Учительские весны&raquo;, обладатель гранта губернатора Краснодарского края.`,
    `2016&nbsp;г. &mdash;&nbsp;курсы повышения квалификации Томского государственного университета.`,
    `2015&ndash;2016&nbsp;г. &mdash;&nbsp;готовила учащихся к&nbsp;научно-практическим конференциям &laquo;Эврика&raquo;, &laquo;Vivat, historia&raquo;.`
  ],
  expirience: [
    `Репетиторство: с&nbsp;2012&nbsp;года, средний балл по&nbsp;истории&nbsp;&mdash; <span class='teacher-card__item-number'>94</span>.`,
    `Проверяла ВПР 11&nbsp;классов.`
  ]
}, {
  name: `Лилия Сайфутдинова`,
  subject: [`Литература`, `Русский язык`],
  subjectCodes: [
    `literature`,
    `russian-language`
  ],
  status: [
    `Составитель`,
    `Эксперт`
  ],
  imgs: [
    `img/teacher-sythutdinova.jpg`,
    `img/teacher-sythutdinova.jpg`
  ],
  education: [
    `В&nbsp;2005 году окончена Краснополянская школа &#8470;&nbsp;2&nbsp;Вятскополянского района Кировской области с&nbsp;серебряной медалью.`,
    `В&nbsp;2010 году окончила филологический факультет ТГГПУ {Татарский Государственный Гуманитарно-Педагогический Университет} по&nbsp;специальности учитель русского языка и&nbsp;литературы.`,
    `В&nbsp;2013 году прошла повышение квалификации в&nbsp;&laquo;Российском новом университете&raquo;.`,
    `В&nbsp;2013 году присвоена&nbsp;I квалификационная категория.`,
    `2010-2012 учёба в&nbsp;аспирантуре КФУ`
  ],
  expirience: [
    `С&nbsp;2010 по&nbsp;настоящее время являюсь действующим учителем русского языка и&nbsp;литературы МБОУ &laquo;Гимназия &#8470;&nbsp;10&raquo; города Казани.`,
    `С&nbsp;начала своей карьеры работаю с&nbsp;выпускными классами. Средний балл ЕГЭ моих учеников&nbsp;&mdash; <span class='teacher-card__item-number'>98</span>`
  ]
}, {
  name: `Владимир Криворотов`,
  subject: [`Обществознание`],
  subjectCodes: [
    `social-science`
  ],
  status: [
    `Составитель`
  ],
  imgs: [
    `img/teacher-krivorotov.jpg`,
    `img/teacher-krivorotov.jpg`
  ],
  education: [
    `В&nbsp;2010 году закончил Южный Федеральный университет {РГУ} исторический факультет; квалификация: &laquo;История. Преподаватель истории&raquo; по&nbsp;специальности: &laquo;История&raquo;`,
    `2011&nbsp;&mdash; профессиональная переподготовка в&nbsp;сфере &laquo;практическая юриспруденция&raquo;`
  ],
  expirience: [
    `Работаю в&nbsp;МБОУ СОШ &#8470;&nbsp;17 5&nbsp;лет в&nbsp;должности учителя истории и&nbsp;обществознания`,
    `Средний балл в&nbsp;11&nbsp;классе&nbsp;&mdash; <span class='teacher-card__item-number'>96</span>.`
  ]
}, {
  name: `Галкина Мария Андреевна`,
  subject: [`Английский язык`],
  subjectCodes: [
    `english-language`
  ],
  status: [
    `Эксперт`
  ],
  imgs: [
    `img/teacher-galkina.jpg`,
    `img/teacher-galkina.jpg`
  ],
  education: [
    `НИЯУ МИФИ, Институт Международных отношений;`,
    `The Business English Certificate (ESOL), The University of&nbsp;Cambridge (2011&nbsp;г.).`
  ],
  expirience: [
    `Репетиторство: 5&nbsp;лет;`,
    `Имеется опыт общения с&nbsp;иностранцами в&nbsp;течение 6&nbsp;лет;`,
    `Средний балл учеников&nbsp;&mdash; <span class='teacher-card__item-number'>94</span>.`
  ]
}];
