const mainHeader = document.querySelector(`.page__header`);
const navMenu = document.querySelector(`.nav`);
const subnavToggleBtn = navMenu.querySelector(`.nav__toggle`);
const subnav = navMenu.querySelector(`.nav__sublist`);

if (subnavToggleBtn) {
  subnavToggleBtn.addEventListener(`click`, (event) => {
    event.preventDefault();
    subnavToggleBtn.classList.toggle(`nav__toggle--open`);
    subnav.classList.toggle(`nav__sublist--hidden`);
    mainHeader.classList.toggle(`page__header--expand`);
  });
}
