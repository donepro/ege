import closest from 'closest';

if (document.querySelector(`.theme`) !== null) {
  let ESCAPE_KEY_CODE = 27;

  const page = document.querySelector(`.page`);
  const classPageNarrow = `page--narrow-nav`;
  let modalVariantBtn = document.querySelector(`.theme__choice-link`);
  let modalVariant = document.querySelector(`.modal`);
  let pageHeader = document.querySelector(`.page__header`);
  let headerLogo = pageHeader.querySelector(`.logo`);
  let mainNav = pageHeader.querySelector(`.nav`);
  let copyright = pageHeader.querySelector(`.copyright`);
  let pageNotification = document.querySelector(`.page__notification`);
  let modalVariantClose = modalVariant.querySelector(`.modal__close`);
  let modalVariantReset = modalVariant.querySelector(`.modal__reset`);
  let changeAmountBtns = document.querySelectorAll(`.theme-card__amount-btn`);
  let selected = [];

  const renewSelected = (checkbox) => {
    let isChecked = checkbox.checked;
    let input = closest(checkbox, `.theme-card`).querySelector(`.theme-card__amount-text`);
    let request = {
      id: checkbox.value,
      amount: input.value
    };
    let selectedRequest = selected.find((el) => el.id === request.id);
    let isNotSelected = typeof selectedRequest === `undefined`;

    if (isChecked) {
      if (isNotSelected) {
        selected.push(request);
      } else {
        selectedRequest.amount = request.amount;
      }
    } else {
      if (!isNotSelected) {
        selected = selected.filter((el) => el.id !== request.id);
      }
    }

    let serialized = selected.map((it) => it.id + `=` + it.amount).join(`&`);

    if (serialized !== ``) {
      let form = closest(checkbox, `.modal__form`);
      let formAction = form.getAttribute(`action`);

      form.setAttribute(`action`, formAction.split(`/mixed/`)[0] + `/mixed/` + serialized);
    }
  };

  const onClickBtn = (btn, isLessBtn = true) => {
    const isModal = typeof closest(btn, `.modal`) !== `undefined`;
    const modalVariantInputMinValue = 5;
    const modalVariantInputMaxValue = 35;
    const modalVariantInputStep = 5;
    let themeCard = closest(btn, `.theme-card`);
    let linkEl = themeCard.querySelector(`.theme-card__link`);
    let input = themeCard.querySelector(`.theme-card__amount-text`);
    let lessBtn = themeCard.querySelector(`.theme-card__amount-btn--less`);
    let moreBtn = themeCard.querySelector(`.theme-card__amount-btn--more`);
    let currentValue = +input.value;
    let newValue = currentValue;
    // default value for less btn
    let isLimitReached = currentValue <= modalVariantInputMinValue + modalVariantInputStep;

    if (isLessBtn) {
      moreBtn.classList.remove(`theme-card__amount-btn--disabled`);

      if (currentValue > modalVariantInputMinValue) {
        newValue -= modalVariantInputStep;
      }
    } else {
      lessBtn.classList.remove(`theme-card__amount-btn--disabled`);

      isLimitReached = currentValue >= modalVariantInputMaxValue - modalVariantInputStep;

      if (currentValue < modalVariantInputMaxValue) {
        newValue += modalVariantInputStep;
      }
    }

    if (linkEl !== null) {
      let link = linkEl.getAttribute(`href`);
      linkEl.setAttribute(`href`, link.split(`=`)[0] + `=` + newValue);
    }


    btn.classList.toggle(`theme-card__amount-btn--disabled`, isLimitReached);
    input.value = newValue;

    if (isModal) {
      let checkbox = themeCard.querySelector(`.theme-card__checkbox`);

      renewSelected(checkbox);
    }
  };

  changeAmountBtns.forEach((it) => it.addEventListener(`click`, () => onClickBtn(it, it.classList.contains(`theme-card__amount-btn--less`))));

  const setThemeAmountText = function () {
    let modalVariantCheckboxesChecked = modalVariant.querySelectorAll(`.theme-card__checkbox:checked`);
    let modalThemeAmountText = document.querySelector(`.modal__number`);

    if (modalVariantCheckboxesChecked.length === 0 || modalVariantCheckboxesChecked.length >= 5) {
      modalThemeAmountText.textContent = `тем`;
    } else if (modalVariantCheckboxesChecked.length === 1) {
      modalThemeAmountText.textContent = `тему`;
    } else {
      modalThemeAmountText.textContent = `темы`;
    }
  };

  const setThemeAmount = function () {
    const onClickCheckbox = (it) => {
      let amountModalChecked = modalVariant.querySelectorAll(`.theme-card__checkbox:checked`);
      let modalThemeAmount = document.querySelector(`.modal__amount`);
      let themesAmount = amountModalChecked.length + ` `;
      modalThemeAmount.textContent = themesAmount;
      let submitBtn = modalVariant.querySelector(`.modal__submit`);
      const classSubmitDisabled = `modal__submit--disabled`;

      submitBtn.classList.toggle(classSubmitDisabled, amountModalChecked.length === 0);

      renewSelected(it);

      setThemeAmountText();
    };

    let modalVariantCheckboxes = modalVariant.querySelectorAll(`.theme-card__checkbox`);

    modalVariantCheckboxes.forEach((it) => it.addEventListener(`click`, () => onClickCheckbox(it)));
  };

  setThemeAmount();

  const resetModalVariant = function () {
    let newModalVariantInputs = modalVariant.querySelectorAll(`.theme-card__amount-text`);

    newModalVariantInputs.forEach((it) => {
      let card = closest(it, `.theme-card`);
      let checkbox = card.querySelector(`.theme-card__checkbox`);
      let lessBtn = card.querySelector(`.theme-card__amount-btn--less`);
      let moreBtn = card.querySelector(`.theme-card__amount-btn--more`);

      checkbox.removeAttribute(`checked`);
      it.setAttribute(`value`, `5`);
      lessBtn.classList.add(`theme-card__amount-btn--disabled`);
      moreBtn.classList.remove(`theme-card__amount-btn--disabled`);
    });

    let modalThemeAmountText = document.querySelector(`.modal__number`);
    let modalThemeAmount = document.querySelector(`.modal__amount`);
    modalThemeAmountText.textContent = `тем`;
    modalThemeAmount.textContent = `0 `;

    setThemeAmount();
  };

  modalVariantReset.addEventListener(`click`, resetModalVariant);

  const toggleModalVariantVisibility = function () {
    modalVariant.classList.toggle(`modal--hidden`);
    page.classList.toggle(classPageNarrow);
    pageHeader.classList.toggle(`page__header--narrow`);
    headerLogo.classList.toggle(`logo--small`);
    mainNav.classList.toggle(`nav--narrow`);
    copyright.classList.toggle(`copyright--hidden`);
    if (pageNotification !== null) {
      pageNotification.classList.toggle(`page__notification--hidden`);
    }
  };

  const closeModalVariant = function (evt) {
    if (evt.keyCode === ESCAPE_KEY_CODE) {
      modalVariant.classList.add(`modal--hidden`);
      page.classList.remove(classPageNarrow);
      pageHeader.classList.remove(`page__header--narrow`);
      headerLogo.classList.remove(`logo--small`);
      mainNav.classList.remove(`nav--narrow`);
      copyright.classList.remove(`copyright--hidden`);
      if (pageNotification !== null) {
        pageNotification.classList.remove(`page__notification--hidden`);
      }
    }
  };

  modalVariantBtn.addEventListener(`click`, function (evt) {
    evt.preventDefault();
    toggleModalVariantVisibility();
    document.addEventListener(`keydown`, closeModalVariant);
  });

  modalVariantClose.addEventListener(`click`, function (evt) {
    evt.preventDefault();
    toggleModalVariantVisibility();
    document.removeEventListener(`keydown`, closeModalVariant);
  });
}
