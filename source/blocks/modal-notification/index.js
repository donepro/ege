const escapeKeyCode = 27;
const modalNotificationToggle = document.querySelector(`.page__notification`);
const modalNotification = document.querySelector(`.modal-notification`);

if (modalNotification !== null) {
  const modalNotificationCrossBtn = modalNotification.querySelector(`.modal-notification__close`);
  const modalNotificationCloseBtn = modalNotification.querySelector(`.modal-notification__btn`);

  document.addEventListener(`keydown`, closeModalNotificationOnEsc);

  const hideModalNotification = function () {
    modalNotification.classList.add(`modal-notification--hidden`);
  };

  const closeModalNotificationOnEsc = function (evt) {
    if (evt.keyCode === escapeKeyCode) {
      hideModalNotification();
    }
  };

  if (modalNotificationCrossBtn) {
    modalNotificationCrossBtn.addEventListener(`click`, (evt) => {
      evt.preventDefault();
      hideModalNotification();
    });
  }

  if (modalNotificationCloseBtn) {
    modalNotificationCloseBtn.addEventListener(`click`, (evt) => {
      evt.preventDefault();
      hideModalNotification();
    });
  }

  if (modalNotificationToggle) {
    modalNotificationToggle.addEventListener(`click`, (evt) => {
      evt.preventDefault();
      modalNotification.classList.remove(`modal-notification--hidden`);
    });
  }
}
