<?php
//header("Content-Type: text/html; charset=utf-8");

// Куда отправлять сообщения
$emailAddressBackup = "qa@donepro.ru";
$emailAddress = "info@podgotovkakege.com";

// Адрес сайта, с которого он отправляет сообщения
$siteEmail = "info@egenasotku.com";

//Адрес на который приходит ответ от клиента
$replyEmail = "info@podgotovkakege.com";

// Тема сообщения
$emailTheme = "New Request from EGE";

//We check data
if($_POST) {

  $text_data = $_POST;

  $message = "";
  $name = isset($text_data["name"]) ? $text_data["name"] : "";
  $message .= "<b>Имя:</b> " . htmlspecialchars($name) . "<br>";
  $email = isset($text_data["email"]) ? $text_data["email"] : "";
  $message .= "<b>Электронный адрес:</b> " . htmlspecialchars($email) . "<br>";
  $interest = isset($text_data["interest"]) ? $text_data["interest"] : "";
  $message .= "<b>Текст обращения</b> " . htmlspecialchars($interest) . "<br>";
  $phone = isset($text_data["phone"]) ? $text_data["phone"] : "";
  $message .= "<b>Телефон:</b> " . htmlspecialchars($phone) . "<br>";

  if (isset($text_data['template'])) {
    $message = $text_data['template'];
    foreach ($text_data as $key => $value) {
      $message = str_replace('{{' . $key . '}}', htmlspecialchars($value), $message);
    }
  }
  if (isset($text_data['theme'])) {
    $emailTheme = $text_data['theme'];
  }
  if (isset($text_data['recipient'])) {
    $emailAddress = $text_data['recipient'];
  }
  if (isset($text_data['sender'])) {
    $siteEmail = $text_data['sender'];
  }
  if (isset($text_data['replyTo'])) {
    $replyEmail = $text_data['replyTo'];
  }

  $headers = array(
    "MIME-Version: 1.0",
    "From: " . $siteEmail,
    "Reply-To: " . $replyEmail,
    "Content-Type: text/html; charset=utf-8"
  );

  if(mail($emailAddress, $emailTheme, $message, implode("\r\n", $headers)) && mail($emailAddressBackup, $emailTheme, $message, implode("\r\n", $headers)))
    exit("{\"type\":\"success\",\"message\":\"Hooray! If you do not receive the email, check spam\"}");
  else
    exit("{\"type\":\"mail\",\"message\":\"Failure! The php does not send the mail!\"}");

} else echo "Ups...";

?>
