// common
import './polyfills';
import './polyfills/picturefill.js';

// blocks
import '../blocks/theme';
import '../blocks/modal-notification';
